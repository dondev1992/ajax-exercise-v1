import SuspectsPage from './pages/SuspectsPage';
import StolenItemsPage from './pages/StolenItemsPage';
import Navbar from './components/Navbar';
import Spinner from './components/Spinner';
import Modal from './components/Modal';
import './App.css';
import { useEffect, useState } from 'react';
import { Route, Routes } from 'react-router-dom';

function App() {
  const [loading, loadingSet] = useState(false);
  const [error, errorSet] = useState(false);
  const [users, usersSet] = useState([]);
  const [items, itemsSet] = useState([]);

  useEffect(() => {
    async function getData() {
      loadingSet(true);
      try {
        let response1 = await fetch('https://641479a1e8fe5a3f3a07a0bb.mockapi.io/users');
        if (!response1.ok) {
          throw new Error();
        }
        const users = await response1.json();

        let response2 = await fetch('https://641479a1e8fe5a3f3a07a0bb.mockapi.io/items');
        if (!response2.ok) {
          throw new Error();
        }
        const items = await response2.json();

        itemsSet(items);
        usersSet(users);
        loadingSet(false);      
      } catch (err) {
        console.log(err.message || err);
        errorSet(true)
        loadingSet(false)
      }
    }
    getData();

  }, [])

  return (
    <div className="App">
      <Navbar />
      <Routes>
        <Route path='/suspects' element={<SuspectsPage users={users} error={error} loading={loading} />} />
        <Route path='/' element={<StolenItemsPage items={items} error={error} loading={loading} />} />
      </Routes>
    </div>
  );
}

export default App;
