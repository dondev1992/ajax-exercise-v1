import classes from './Card.module.css';

const Card = ({ user }) => {
    return (
        <div className={classes.card_container}>
            <img src={user?.image} alt="user" />
            <div className={classes.user_description}>
                <h3>{user?.name}</h3>
                <p>{user?.city}, {user?.state}</p>
            </div>

        </div>
    )
}

export default Card