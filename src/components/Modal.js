import classes from './Modal.module.css';

const Modal = ({ error }) => {
    if (!error) {
        return null;
    }
  return (
    <div className={classes.container}>
        <div className={classes.modal_container}>
            <h1>Oops something went wrong!</h1>
        </div>
        
    </div>
  )
}

export default Modal