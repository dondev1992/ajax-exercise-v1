import classes from './Item.module.css';

const Item = ({ item }) => {
    return (
        <div className={classes.item_container}>
            <img src={item?.vehicle_image} alt="item" />
            <div className={classes.item_description}>
                <h3>{item?.vehicle_make} {item?.vehicle_model}</h3>
                <strong><p>(stolen)</p></strong>
            </div>
        </div>
    )
}

export default Item