import { NavLink } from 'react-router-dom';
import classes from './Navbar.module.css';

const Navbar = () => {

    const activeLink = {
        color: '#124559',
        fontWeight: 'extraBold',
        borderBottom: '2px solid #124559'
    }
    return (
        <nav className={classes.nav_container}>
            <h2>Hacker Daily News</h2>
            <div className={classes.nav_link}>
                <NavLink style={({ isActive }) => { return isActive ? activeLink : {} }} to='/'>Stolen</NavLink>
                <NavLink style={({ isActive }) => { return isActive ? activeLink : {} }} to='/suspects'>Suspects</NavLink>
            </div>
        </nav>
    )
}

export default Navbar