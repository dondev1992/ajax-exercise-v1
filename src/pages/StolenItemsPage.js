import classes from './StolenItemsPage.module.css';
import Item from '../components/Item';
import Spinner from '../components/Spinner';
import Modal from '../components/Modal';

const StolenItemsPage = ({ items, error, loading }) => {
    return (
        <div>
            {error && <Modal error={error} />}
            {loading ? <Spinner /> :
                <div>
                    <h1 className={classes.title}>Stolen Items</h1>
                    <div className={classes.list_container}>
                        {items?.map((item) => (
                            <div key={item.id}>
                                <Item item={item} />
                            </div>
                        ))}
                    </div>
                </div>
            }
        </div>
    )
}

export default StolenItemsPage