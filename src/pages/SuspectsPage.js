import classes from './SuspectsPage.module.css';
import Card from '../components/Card';
import Spinner from '../components/Spinner';
import Modal from '../components/Modal';

const SuspectsPage = ({ users, error, loading }) => {

    return (
        <div>
            {error && <Modal error={error} />}
            {loading ? <Spinner /> :
                <div>
                    <h1 className={classes.title}>The Usual Suspects</h1>
                    <div className={classes.list_container}>
                        {users?.map((user) => (
                            <div key={user.id}>
                                <Card user={user} />
                            </div>
                        ))}
                    </div>
                </div>
            }
        </div>
    )
}

export default SuspectsPage